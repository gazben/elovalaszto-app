<?php
use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '45ce08d16cd3dadee0f45886bff22dabfcf4f1f95196b19459b847621632a293',
                'zip' => '1212',
                'created' => '2019-05-21 15:17:54',
                'created_by' => '',
                'place_created' => '1',
                'modified' => '2019-06-06 11:46:18',
                'lastletters' => 'BU',
                'mobile' => '',
                'email' => 'admin@ahang.hu',
                'signer_email_hash' => '50f5b4ba2289dd12b91bbc14b8fc74c72819c0aaac431da8b5490c2b04c0b364',
                'mobile_marketable' => '0',
                'email_marketable' => '0',
                'birthdate' => '2019-05-21',
                'acceptance' => '1',
                'verified' => NULL,
                'bizottsag' => NULL,
                'valid' => '0',
                'voted' => NULL,
                'vote_time' => NULL,
                'place_voted' => '0',
                'username' => 'admin',
                'password' => '$2y$10$0tx2tnCDC.d/iY6HOJLvsuMIUhhjjPNKz7oBzLESfA0gEHznsLCSq',
                'role' => '3',
                'pin' => NULL,
                'pin_ok' => '1',
                'anonymized' => NULL,
                'anonymized_at' => NULL,
            ],

        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
