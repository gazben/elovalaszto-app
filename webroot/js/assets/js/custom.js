/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

$(document).ready(function(){
    // Show modal on page load
    $("#elovalasztoModal").modal('show');

    // Hide modal on button click
    $(".close").click(function(){
        $("#elovalasztoModal").modal('hide');
    });

    // Paginator
    $("ul.pagination.elovalaszto li").addClass('page-item');
    $("ul.pagination.elovalaszto li a").addClass('page-link');
});