<?php
namespace App\Policy;

use App\Model\Entity\User;
use Authorization\IdentityInterface;
use Cake\Core\Configure;
use DateTime;

/**
 * User policy
 */
class UserPolicy
{
    /**
     * Check if $user can create User
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\User $resource
     * @return bool
     */
    public function canOfflinereg(IdentityInterface $user, User $resource)
    {
        return true;
    }

    public function canBejelentkezes() {
        return true;
    }

    public function canPin() {
        return true;
    }

    public function canMyself(IdentityInterface $user, User $resource) {
        if ($resource->id === $user->id) {
            return true;
        } else {
            return false;
        }

    }

    public function canVoteOnline(IdentityInterface $user)
    {
        $vote_start = Configure::read('vote.start');
        $vote_end = Configure::read('vote.end');
        if (
            $user->verified &&  // ellenőrzött (sátras regisztráló vagy pdf-es)
            $user->valid &&  // az ellenőrzés sikeresen lefutott
            !$user->voted && // még nem szavazott
            ($user->role === 0) && // sima szavazó státuszú
            $user->pin_ok && // pin kódját visszaigazolta
            (new DateTime('now') > new DateTime($vote_start)) && // már elindult a szavazás TODO: visszarakni teszt után
            (new DateTime('now') < new DateTime($vote_end)) // már elindult a szavazás TODO: visszarakni teszt után

        ) {
            return true;
        } else {
            return false;
        }

    }



    public function canAdmin(IdentityInterface $user) {
        if ($user->role === 3) { // Only admins can register volounteers
            return true;
        } else  {
            return false;
        }
    }

    public function canVolounteer(IdentityInterface $user) {
        if ($user->role === 1) { // Only admins can register volounteers
            return true;
        } else  {
            return false;
        }
    }

    public function canFiok(IdentityInterface $user, User $resource) {
        if ($user->id === $resource->id) { // csak a saját fiókodat tudod megtekinteni
            return true;
        } else  {
            return false;
        }

    }

    public function canValidateDocument(IdentityInterface $user) {
        if ($user->role === 2) {
            return true;
        } else  {
            return false;
        }
    }

    /**
     * Check if $user can update User
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\User $resource
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, User $resource)
    {
    }

    /**
     * Check if $user can delete User
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\User $resource
     * @return bool
     */
    public function canDelete(IdentityInterface $user, User $resource)
    {
    }

    /**
     * Check if $user can view User
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\User $resource
     * @return bool
     */
    public function canView(IdentityInterface $user, User $resource)
    {
    }
}
