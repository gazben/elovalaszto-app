<?php
namespace App\Model\Entity;
use Authentication\IdentityInterface as AuthenticationIdentity;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Authorization\AuthorizationService;
use Authorization\IdentityInterface as AuthorizationIdentity;
use Cake\ORM\Entity;
use DateTime;
/**
 * User Entity
 *
 * @property string $id
 * @property int $zip
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $lastletters
 * @property \Cake\I18n\FrozenDate $date_of_birth
 * @property bool $acceptance
 * @property string|null $verified
 * @property bool|null $valid
 * @property bool|null $voted
 * @property \Cake\I18n\FrozenTime|null $vote_time
 * @property string $username
 * @property string $password
 */
class User extends Entity implements AuthorizationIdentity, AuthenticationIdentity
{
    /**
     * Authentication\IdentityInterface method
     */
    public function getIdentifier()
    {
        return $this->id;
    }

    /**
     * Authentication\IdentityInterface method
     */
    public function getOriginalData()
    {
        return $this;
    }

    /**
     * Authorization\IdentityInterface method
     */
    public function can($action, $resource)
    {
        return $this->authorization->can($this, $action, $resource);
    }

    /**
     * Authorization\IdentityInterface method
     */
    public function applyScope($action, $resource)
    {
        return $this->authorization->applyScope($this, $action, $resource);
    }

    /**
     * Setter to be used by the middleware.
     */
    public function setAuthorization(AuthorizationService $service)
    {
        $this->authorization = $service;

        return $this;
    }

    protected function _setPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();
            return $hasher->hash($value);
        }
    }
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'zip' => true,
        'created' => true,
        'modified' => true,
        'lastletters' => true,
        'birthdate' => true,
        'email' => true,
        'mobile_marketable' => true,
        'email_marketable' => true,
        'acceptance' => true,
        'verified' => true,
        'valid' => true,
        'voted' => true,
        'vote_time' => true,
        'username' => true,
        'password' => true,
        'role' => true,
        'pin' => true,
        'pin_ok' => true,
        'anonymized' => true,
        'anonymized_at' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
