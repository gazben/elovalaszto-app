<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Utility\Text;
use Cake\ORM\Table;
use Cake\Core\Configure;
use Cake\Validation\Validator;
use DateTime;
use Cake\Mailer\Email;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('username');
        $this->setPrimaryKey('id');
        $this->belongsTo('Places', [
            'foreignKey' => 'place_voted',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('id')
            ->requirePresence('username', 'create')
            ->allowEmptyString('username', false)
            ->maxLength('id', 255)
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'Ezzel a lakcímkártyaszámmal már regisztráltak a rendszerben.']);

        $validator
            ->integer('zip')
            ->add('zip', 'custom', [
                'rule' => function ($value, $context) {
                    if (!in_array($value, [1069,1011,1012,1013,1014,1015,1016,1021,1022,1023,1024,1025,1026,1027,1028,1029,1031,1032,1033,1034,1035,1036,1037,1038,1039,1041,1042,1043,1044,1045,1046,1047,1048,1051,1052,1053,1054,1055,1056,1061,1062,1063,1064,1065,1066,1067,1068,1071,1072,1073,1074,1075,1076,1077,1078,1081,1082,1083,1084,1085,1086,1087,1088,1089,1091,1092,1093,1094,1095,1096,1097,1098,1101,1102,1103,1104,1105,1106,1107,1108,1111,1112,1113,1114,1115,1116,1117,1118,1119,1121,1122,1123,1124,1125,1126,1131,1132,1133,1134,1135,1136,1137,1138,1139,1141,1142,1143,1144,1145,1146,1147,1148,1149,1151,1152,1153,1154,1155,1156,1157,1158,1161,1162,1163,1164,1165,1171,1172,1173,1174,1181,1182,1183,1184,1185,1186,1188,1191,1192,1193,1194,1195,1196,1201,1202,1203,1204,1205,1211,1212,1213,1214,1215,1221,1222,1223,1224,1225,1237,1238,1239,1529])) {
                        return false;
                    } else {
                        return true;
                    }
                },
                'message' => __('Az irányítószám nem megfelelő, csak budapesti irányítószámok adhatók meg!')
            ])
            ->requirePresence('zip', 'create')
            ->allowEmptyString('zip', false);

        $validator
            ->scalar('lastletters')
            ->maxLength('lastletters', 2)
            ->requirePresence('lastletters', 'create')
            ->allowEmptyString('lastletters', false);
        $validator
            ->scalar('docnum')
            ->requirePresence('docnum', 'create')
            ->allowEmptyString('docnum', false);




        $validator
            ->date('birthdate')
            ->add('birthdate', 'custom', [
                // Emberek szempontjából értelmezhető cél
                // Kilépés: megszűnt civilnek lenni a történet
                'rule' => function ($value, $context) {
                    if (new DateTime($value['year']. '-' . $value['month']. '-' . $value['day']) < new DateTime('2001-10-01')) {
                        return true;
                    } else {
                        return false;
                    }
                },
                'message' => __('Az előválasztáson csak 2001 október 1. előtt születettek vehetnek részt')
            ])
            ->requirePresence('birthdate', 'create')
            ->allowEmptyDate('birthdate', false);

        $validator
            ->add('acceptance', 'custom', [
                'rule' => function ($value, $context) {
                    if ($value != 1){
                        return false;
                    } else {
                        return true;
                    }
                },
                'message' => __('A felhasználási feltételeket el kell fogadni a használathoz')
            ]);

        $validator
            ->scalar('verified')
            ->maxLength('verified', 255)
            ->allowEmptyString('verified');

        $validator
            ->boolean('valid')
            ->allowEmptyString('valid');

        $validator
            ->boolean('voted')
            ->allowEmptyString('voted');

        $validator
            ->dateTime('vote_time')
            ->allowEmptyDateTime('vote_time');

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->allowEmptyString('username', false)
            ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __('Válassz másik felhasználónevet, ez már foglalt!')]);

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->allowEmptyString('password', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        return $rules;
    }

    public function saveWithMessage($user, $message = null, $subject = null) {
        if ($result = $this->save($user)) {
            $this->__notifyUser($user, $message, $subject);
            return $result;
        } else {
            return false;
        }
    }

    public function __notifyUser($user, $message = null, $subject = null) {
            $email = new Email('default');
            $email->setFrom(['elovalaszto@ahang.hu' => 'Előválasztó'])
            ->setEmailFormat('text')
            ->setTo($user->email)
            ->setSubject($subject);
            if ($email->send($message)) {
                return true;    
            } else {
                return false;    
            }
            ;
            
    }

    public function canVote ($id = null) {
        $vote_start = Configure::read('vote.start');
        $vote_end = Configure::read('vote.end');
        $reasons = [];
        if (new DateTime('now') < new DateTime($vote_start)) {
            array_push($reasons, __('Még nem nyitottuk meg a szavazást, csak a regisztrációt. Szavazni {0}-tól lehet!', $vote_start));
            // return($reasons);
        }
        if (new DateTime('now') > new DateTime($vote_end)) {
            array_push($reasons, __('A szavazás már lezárásra került. Szavazni {0}-ig lehetett!', $vote_end));
            // return($reasons);
        }


        $user = $this->get($id);

        if (!$user->pin_ok) array_push($reasons, __('Nem erősítetted meg az e-mail címedet a kiküldött PIN kóddal. Megoldás: <a href="/users/newpin">Új pin kód kérése</a>'));
        if ($user->role != 0 ) array_push($reasons, __('Csak szavazók szavazhatnak!'));
        if (!$user->verified) array_push($reasons, __('Az aHang aktivistái még nem ellenőrizték az általad megadott adatokat. Kérjük, lépj velünk kapcsolatba!'));
        if (!$user->valid) array_push($reasons, __('A regisztrációd még nem került elfogadásra. Kérjük, lépj velünk kapcsolatba!'));
        if ($user->voted) {
                $place = $this->Places->get($user->place_voted);
                array_push($reasons, __('A rendszer nyilvántartása szerint ezzel a lakcímkártyával már szavazás történt ekkor: ' . $user->vote_time . __(', itt: ' . $place->name)));}
        // if ($user->birthdate < new DateTime('2001-10-01')) array_push($reasons, __('Csak 2001-09-30 előtt születettek vehetnek részt az előválasztáson.'));
        if (!empty($reasons)) {return $reasons; } else {return true; }

    }

    public function anonymize($id = null) {
        $user = $this->get($id);
            // $user->username = null;
            // $user->password = null;
            $user->email = null;
            $user->email_marketable = false;
            $user->mobile = null;
            $user->mobile_marketable = false;
            $user->anonymized = true;
            $user->anonymizedAt = new DateTime('now');
            if ($result = $this->save($user)) {
                return $result;
            } else {
                return false;
            }
    }
}
