    <section class="section">
    	          <div class="section-header">
            <h1><?php echo __('Bejelentkezés') ?></h1>
          </div>
      <div class="container mt-5">
        <div class="row elovalasztas-h50end">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="card card-primary">
              <div class="card-body">
              	<?php echo $this->Form->create($user, ['class' => 'needs-validation']); ?>
                  <div class="form-group">
                  	<?php echo $this->Form->control('username', ['label' => __('Felhasználónév'), 'class' => 'form-control', 'tabindex' => '1', 'required', 'autofocus']); ?>
                  </div>
                  <div class="form-group">
                    <div class="d-block">
                      <div class="float-right">
                          <?php echo $this->Html->link('Elfelejtett jelszó?', ['action' => 'resetPassword'], ['class' => 'text-small']); ?>
                        </a>
                      </div>
                    </div>
                    <?php echo $this->Form->control('password', ['label' => __('Jelszó'), 'class' => 'form-control', 'tabindex' => '2', 'required']); ?>
                  </div>
                  <div class="form-group">
                      <?php echo $this->Form->button(__('Bejelentkezés'), ['class' => 'btn btn-primary btn-lg btn-block', 'tabindex' => '4']); ?>
                  </div>
                <?php echo $this->Form->end(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>