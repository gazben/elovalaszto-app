<?php 

                $this->Form->setTemplates([
                'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}} class="label-inline">{{text}}</label>',
                'formGroup' => '{{input}}{{label}}',
                ]);
 ?>


        <section class="section">
          <div class="section-header">
            <h1>Szavazó személyes szavazáson megjelent</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
              	<!-- </p> -->
              	<?php
                echo $this->Form->create($user);
                echo $this->Form->control('zip', ['label' => 'Irányítószám']);
                echo $this->Form->control('docnum', ['label' => 'Lakcímkártya száma'] );
                echo $this->Form->control('birthdate', ['type'=> 'date', 'label' => 'Születési dátum', 'minYear' => '1920', 'maxYear' => '2001', 'default' => new DateTime('2001-09-30')]);
                echo $this->Form->control('email', ['label' => 'E-mail cím, nem kötelező megadni']);
                echo $this->Form->control('email_marketable', ['type'=>'checkbox', 'label' => __('Szeretne értesítéseket kapni az előválasztás fejleményeiről és az aHang kezdeményezéseiről emailben (ez nagyon fontos, mert csak így tudunk vele kapcsolatban maradni).')]);

                echo $this->Form->control('acceptance', ['label' => 'Felhasználási feltételeket és adatkezelési nyilatkozatot elfogadta. (ld. kifüggesztve a sátor falán)']);
                echo $this->Form->button('Odaadom a szavazólapot', ['class' => 'btn btn-outline-primary']);
                echo $this->Form->end();
                ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>
          </div>
        </section>
<script>
  $(document).ready(function(){
    $("body").addClass('addVolounteer');
    $("body.elovalaszto.addVolounteer input").addClass('form-control');
    $("body.elovalaszto.addVolounteer select").addClass('form-control');
});  

</script>
