        <section class="section">
          <div class="section-header">
            <h1><?php echo __('Új jelszó beállítása') ?></h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
              	<!-- </p> -->
              	<?php echo $this->Form->create($user); ?>
              	<?php echo $this->Form->control('username', ['label' => __('Kérlek adja meg a regisztráláskor választott felhasználónevét!'), 'class' => 'form-control', 'required', 'autofocus']); ?>
              	<div style="text-align: center; margin-top: 20px;"><?php echo $this->Form->submit(__('Tovább'), ['class' => 'btn btn-outline-primary']); ?></div>
              	<?php echo $this->Form->end(); ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>
          </div>
        </section>