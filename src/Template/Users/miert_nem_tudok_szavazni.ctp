        <section class="section">
          <div class="section-header">
            <h1>Miért nem tudok szavazni?</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
              	<!-- </p> -->
              	<?php if (!empty($reasons)): ?>
              		<p>A következő okok miatt hiúsult meg a szavazási kísérleted:</p>
              		<ul>
              			<?php foreach ($reasons as $reason): ?>
              				<li><?php echo $reason ?></li>
              			<?php endforeach ?>
              		</ul>
              	<?php else :  ?>
              	<p>Úgy tűnik, hogy tudsz szavazni! Ha mégis úgy tapasztalod, hogy nem, lépj velünk kapcsolatba!</p>
              	<?php endif ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>
          </div>
        </section>