<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Place[]|\Cake\Collection\CollectionInterface $places
 */
?>
        <section class="section">
          <div class="section-header">
            <h1><?= __('Places') ?></h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->

            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body" style="margin: 0 auto;">
                <!-- </p> -->
                <?= $this->Html->link(__('New Place'), ['action' => 'add'], ['class' => 'btn btn-primary margin-right-15 under-500 under-500-marginbottom-15']) ?>
                <?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn btn-primary margin-right-15 under-500 under-500-marginbottom-15']) ?>
                <?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-primary under-500']) ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>

            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
                <!-- </p> -->
        <table class="table table-striped" style="-webkit-border-radius: .25rem; -moz-border-radius: .25rem; border-radius: .25rem; overflow: hidden;">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($places as $place): ?>
                <tr>
                    <th scope="row"><?= $this->Number->format($place->id) ?></th>
                    <td><?= h($place->name) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $place->id], ['class' => 'btn btn-outline-info btn-sm']) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $place->id], ['class' => 'btn btn-outline-info btn-sm']) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $place->id], ['class' => 'btn btn-outline-info btn-sm'], ['confirm' => __('Are you sure you want to delete # {0}?', $place->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="buttons">
            <nav aria-label="Page navigation">
                <ul class="pagination elovalaszto" style="margin-top: 2rem;">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
            </nav>
        </div>
              <!-- </p> -->
              </div>
              <div class="card-footer bg-whitesmoke">
                <?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?>
              </div>
            </div>

          </div>
        </section>