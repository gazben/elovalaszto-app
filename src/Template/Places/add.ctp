<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Place $place
 */
?>

        <section class="section">
          <div class="section-header">
            <h1>Szavazóhely hozzáadása</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
                <!-- </p> -->
                <?php
                echo $this->Form->create($place);
                echo $this->Form->control('name', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;', 'label' => 'Név:']);?>
                <div style="text-align: center; margin-top: 20px;"><?php echo $this->Form->button(__('Hely mentése'), ['class' => 'btn btn-outline-primary']); ?></div>
                <?php echo $this->Form->end();
                ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>
          </div>
        </section>
