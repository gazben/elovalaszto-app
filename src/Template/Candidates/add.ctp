<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Candidate $candidate
 */
?>
        <section class="section">
          <div class="section-header">
            <h1><?= __('Add Candidate') ?></h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
                <!-- </p> -->
                <?= $this->Form->create($candidate) ?>
                <fieldset>
                    <?php
                    echo $this->Form->control('name', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;']);
                    echo $this->Form->control('party', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;']);
                    ?>
                </fieldset>
                <div style="text-align: center; margin-top: 20px;"><?= $this->Form->button(__('Submit'), ['class' => 'btn btn-outline-primary']); ?></div>
                <?= $this->Form->end() ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>
          </div>
        </section>