    <section class="section">
    	          <div class="section-header">
            <h1><?php echo __('Sikeres szavazás') ?></h1>
          </div>
      <div class="container mt-5">
        <div class="row elovalasztas-h50end">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="card card-primary">
              <div class="card-body">
              	Köszönjük, hogy igénybe vetted az előválasztót.
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>