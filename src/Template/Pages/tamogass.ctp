        <section class="section">
          <div class="section-header">
            <h1><?php echo __('Támogass minket!') ?></h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->

            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
              	<!-- </p> -->
                <p>Az előválasztás költségei fedezetének megteremtése érdekében az mindenkit arra kérünk, hogy akinek fontos az előválasztás és megengedheti magának - anyagilag támogassa az ügyet.</p>

                <p><strong>A magyarHang Nonprofit Kft. a befizetésekből finanszírozza az előválasztás során felmerülő költségeket - a sátrak bérlését, a szoftver fejlesztést, az önkéntesek étkeztetését és megannyi egyéb költséget. Ez a választás - ha jól csináljuk - kiemelkedő példa lehet a magyar demokrácia történetében, amit civilek szerveztek meg civilek pénzéből civileknek.</strong></p>

                <p>Kétféleképpen tud minket anyagilag támogatni.</p>

              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>

            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
                <!-- </p> -->

                <p><strong>1. Paypalon keresztül, vagy bankkártyával</strong>, ezen az oldalon található összegekre kattintva vagy egyéb összeg megadásával:</p>

                <p><a target="_blank" href="https://terjed.ahang.hu/campaigns/elovalasztas?action=donate" class="btn btn-secondary" style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: 100%; margin: 0 auto;">https://terjed.ahang.hu/campaigns/elovalasztas?action=donate</a></p>

              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>

            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
                <!-- </p> -->

                <p><strong>2. Banki átutalással:</strong></p>
                <p><span style="display: block; width: 100%; text-align: center; margin: 0 8px 10px 0; border: 1px solid #cd2a3e; color: #cd2a3e; font-weight: 600; font-size: 12px; line-height: 24px; padding: .3rem .8rem; letter-spacing: .5px; border-radius: .25rem;">Magyarhang Nonprofit Kft. Magnet Bank: 16200223-10108518.</span></p>

                <p>Nemzetközi átutalás esetén:</p>

                <p><span style="display: block; width: 100%; text-align: center; margin: 0 8px 10px 0; border: 1px solid #cd2a3e; color: #cd2a3e; font-weight: 600; font-size: 12px; line-height: 24px; padding: .3rem .8rem; letter-spacing: .5px; border-radius: .25rem;">IBAN: HU42 1620 0223 1010 8518 0000 0000.</span></p>
                <p>A közlemény rovatba kérjük, hogy írja bele: 'Előválasztás'.</p>

              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>

            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
                <!-- </p> -->
                <p><strong>Önkéntes munkával segítenék!</strong></p>

                <p>Nagyon-nagyon kell még a segítség. Tisztességes, elkötelezett demokratákat keresünk, akik nem dolgoznak semelyik jelölt kampánycsapatában, de fontos nekik a budapesti előválasztás folyamata. Az önkéntesek jelentkezését az alábbi kérdőív kitöltésén keresztül várjuk:</p>

                <p><a target="_blank" href="https://terjed.ahang.hu/surveys/ev-onkentes" class="btn btn-primary" style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: 100%; margin: 0 auto;">https://terjed.ahang.hu/surveys/ev-onkentes</a></p>

              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>

          </div>
        </section>