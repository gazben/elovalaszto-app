        <section class="section">
          <div class="section-header">
            <h1>A Főpolgármesteri Előválasztás folyamata</h1>
          </div>
          <div class="section-body">
            <h2 class="section-title">Tudnivalók</h2>
            <p class="section-lead">Civil Választási Bizottság</p>
            <div class="card">
                  <!-- <div class="card-header">
                    <h4>Fej</h4>
                  </div> -->
                  <div class="card-body">
                    <div id="accordion">

                      <div class="accordion">
                        <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#panel-body-1" aria-expanded="false">
                          <h4>Kezdeményezés és kezdeményezők</h4>
                        </div>
                        <div class="accordion-body collapse" id="panel-body-1" data-parent="#accordion" style="">
                          <p class="alert alert-light"><strong>Legyen a demokrácia ünnepe a júniusi főpolgármesteri előválasztás!</strong> Álljanak ki egymás ellen és a választóikért Budapest jelenlegi főpolgármesterének, Tarlós Istvánnak a kihívói! Bizonyítsák rátermettségüket, elhivatottságukat: érveljenek, vitázzanak, kampányoljanak és végül méressenek meg, hogy a fővárosiaknak valódi választása lehessen októberben!</p>

                          <p class="alert alert-light"><strong>Mindehhez egy pártoktól független szervezet adhat tiszta és hiteles keretet.</strong></p>

                          <p class="alert alert-light">Az aHang éppen azért vállalta a budapesti előválasztás koordinálását, amiért bő egy évvel ezelőtt létrejött: <i>Meggyőződésünk, hogy új utakat kell nyitni a változás eléréséhez! Hiszünk abban, hogy mi magunk formálhatjuk a jövőnket, és ezért lépéseket kell tennünk. Valljuk, hogy az új, digitális eszközök épp olyan fontosak a demokratikus tér növelésében, mint a hagyományos elemek - szeretnénk minden korosztályt és csoportot megszólítani, aktivitásra buzdítani.</i></p>

                          <p class="alert alert-light">Áprilisban tagságunkból több mint 50 ezer embernek tettük fel a kérdést: belevágjunk? A válaszadók 95 százaléka igennel felelt arra vonatkozóan, hogy vállaljuk vezető szerepet az előválasztás lebonyolításában.</p>

                          <p class="alert alert-light"><strong>Az aktivisták jelöltek és az őket támogató pártok 2019. májusában létrehozták a Civil Választási Bizottságot, így kiegészült és formalizálódott a szervezők köre, annak érdekében, hogy az előválasztás fordulója során civil kontroll mellett kerüljön kiválasztásra a változást akaró többség főpolgármester-jelöltje.</strong></p>

                          <p class="alert alert-warning">A Civil Választási Bizottság (CVB) tagjai és tagszervezetei:</p>
                          <ul>
                            <li>A CVB elnöke: Dr. Magyar György, az MMM elnökségi tagja</li>
                          </ul>
                          <p class="alert alert-warning">A CVB jelenlegi tagszervezetei:</p>
                          <ul>
                            <li>Civilek a demokráciáért</li>
                            <li>Mindenki Magyarországa Mozgalom</li>
                            <li>Nyomtass te is!</li>
                            <li>aHang Platform</li>
                            <li>Oktogon Közösség</li>
                            <li>Kálmán Olga kampánystábja</li>
                            <li>Karácsony Gergely kampánystábja</li>
                            <li>Kerpel-Fronius Gábor kampánystábja</li>
                            <li>Szolidaritás Mozgalom</li>
                            <li>Számoljuk együtt Mozgalom</li>
                            <li>DK</li>
                            <li>Jobbik</li>
                            <li>LMP</li>
                            <li>Momentum</li>
                            <li>MSZP</li>
                            <li>Párbeszéd</li>
                          </ul>
                          <p class="alert alert-light">A Budapesti Előválasztás lebonyolítását a jelölteket támogató pártközösségek aktivistákkal és infrastrukturális felajánlásokkal segítik. A civilszervezetek (Civilek a demokráciáért, Nyomtass te is, Számoljuk Együtt Mozgalom, aHang) a szervezésben és a lebonyolításban is biztosítják maguk részéről a pártoktól független, civil szavazóbizottsági- és szavazatszámláló tagokat. Pártoktól függetlennek számít az a civilszervezet, amely nem támogat egyetlen jelöltet sem az előválasztáson, annak kampányában nem vesz részt. Az előválasztás technikai lebonyolítását az aHang végzi</p>

                          <p class="alert alert-light">Az elmúlt egy évben számos kampányt indítottunk partnereinkkel vagy saját erőnkből: Hangot adtunk az otthonápolóknak, a kilakoltatottaknak, a dolgozóknak - a sort hosszan folytathatnánk.</p>

                          <p class="alert alert-light">Most is az általunk képviselt polgárok Hangját szeretnénk felerősíteni azzal, hogy keretet, lehetőséget teremtünk a főpolgárpolgármester-jelöltek bemutatkozására, megmérettetésére. Tagjaink (akik korábban valamely kampányunkhoz csatlakoztak) többsége ezt felismerte, és felmérésünk alapján támogatja a felvetést, hogy az aHang bonyolítsa le a főpolgármesteri előválasztást.</p>

                          <p class="alert alert-light"><strong>Nemcsak szervezni, de tematizálni is kívánjuk a folyamatot:</strong> mindazon jelöltek, akik felhívásunkra jelentkeznek és részt vesznek az előválasztásban, vállalják, hogy az a választók és az aHang tagsága által meghatározott kérdésekben kifejtik és vitára bocsátják programjukat.</p>

                          <p class="alert alert-light">Az aHang olyan <strong>szavazási rendszert</strong> alkalmaz, amelynek hitelessége megkérdőjelezhetetlen, mindezt a nyílt forráskód is biztosítja. <a href="https://gitlab.com/elovalaszto/" target="_blank"><strong>A teljes fejlesztésünket dokumetáltuk és ingyenes elérhetővé tettük bárki számára.</strong></a> A jelöltállítási folyamat kidolgozásában figyelembe vettük az eddig látható potenciális jelöltek igényeit és azokból kiindulva dolgoztuk ki saját feltételrendszerünket, amelyet a felhívásunkra jelentkező aspiránsok elfogadnak.</p>

                          <p class="alert alert-light">Teremtsük meg, szerezzük vissza annak az esélyét, hogy választott képviselőink ténylegesen választottak legyenek, ténylegesen minket képviseljenek és valóban a választók által felvetett problémákra reflektálnak!</p>

                          <p class="alert alert-light"><strong>Add a szavazatod, visszaadjuk aHangod!</strong></p>

                          <p class="alert alert-light"><strong>Az előválasztás költségei</strong> fedezetének megteremtése érdekében <a href="https://elovalaszto.hu/pages/tamogass" target="_blank"><strong>a MagyarHang Nonprofit Kft. egy külön bankszámlát nyit és kér mindenkit, akinek fontos az előválasztás, hogy befizetésével támogassa az ügyet</strong></a>. Az aHang a befizetésekből finanszírozza az előválasztás során felmerülő költségeket. A számla egyenlegeit a MagyarHang Nonprofit Kft. nyilvánosságra fogja hozni.</p>
                        </div>
                      </div>

                      <div class="accordion">
                        <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#panel-body-2" aria-expanded="false">
                          <h4>Ki szavazhat?</h4>
                        </div>
                        <div class="accordion-body collapse" id="panel-body-2" data-parent="#accordion" style="">
                          <ul>
                            <li>budapesti lakcímkártyával rendelkezik,</li>
                            <li>2001. október 1. előtt született,</li>
                            <li>elfogadja és aláírásával az ellenjegyzi a magyar jogszabályoknak megfelelően, hogy az előválasztást kezdeményező civilek és pártok a választás lebonyolítása alatt és érdekében tárolják és kezeljék személyes adatait 2019. június 26-ig bezárólag.</li>
                        </ul>
                        </div>
                      </div>

                      <div class="accordion">
                        <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#panel-body-3" aria-expanded="false">
                          <h4>Jelöltállítás</h4>
                        </div>
                        <div class="accordion-body collapse" id="panel-body-3" data-parent="#accordion" style="">
                          <p class="alert alert-light">Jelöltet ajánlhat (akár több jelöltet is) minden budapesti előválasztó, aki adatait (név, születési idő, cím, telefonszám, aláírás) megadja egy (vagy több) jelölt ajánlásgyűjtő ívén, aláírásával az ajánlásgyűjtő íven ellenjegyzi illetve elfogadja, hogy az így begyűjtött ajánlásokat a Civil Választási Bizottság megbízásából utólag az aHang (Magyarhang Nonprofit Kft., cím: 1071 Budapest, Damjanich u. 52. IV. em. 1., Cégjegyzékszám: 01-09-320321, Adószám: 26253851-1-42) szúrópróbaszerűen, telefonos megkeresés útján ellenőrizheti.</p>

                          <p class="alert alert-light">Az ajánlóíveken keresztül közölt adatokat az ellenőrzést végző aHang (Magyarhang Nonprofit Kft.) adathordozón, számítógépen vagy bármilyen elektronikus úton <strong>nem tárolja és az ajánlásokat az ellenőrzési folyamat végén, de legkésőbb 2019. június 26-áig megsemmisíti.</strong></p>
                        </div>
                      </div>

                      <div class="accordion">
                        <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#panel-body-4" aria-expanded="false">
                          <h4>A jelöltté válás feltételei</h4>
                        </div>
                        <div class="accordion-body collapse" id="panel-body-4" data-parent="#accordion" style="">

                          <p class="alert alert-light">2019. június 17. 16.00 óráig minimum 2000 budapesti választópolgár érvényes ajánló aláírásának bemutatása a Civil Választási Bizottság (CVB) számára. Az ajánló aláírások érvényességét a CVB megbízása alapján az aHang (Magyarhang Nonprofit Kft., cím: 1071 Budapest, Damjanich u. 52. IV. emelet 1., Cégjegyzékszám: 01-09-320321, Adószám: 26253851-1-42) végzi. A leadott ajánlások összességéből 5%-os véletlenszerű mintát vesz az aHang önkéntes szerződéssel, titoktartási nyilatkozattal rendelkező aktivista csapata. Az 5%-os minta alapján keletkező érvénytelen ajánlások 20-szorosa kerül levonásra az összes beadott ajánlásból, s ha az így kapott ajánlások száma eléri a 2000-et, akkor a jelölti nyilatkozat aláírását követően hivatalosan jelöltnek minősül a jelentkező.</p>

                          <p class="alert alert-light">A kötelezően használandó ajánlóív az alábbi linkről tölthető le: <a target="_blank" class="text-secondary" href="https://ahang.hu/wp-content/uploads/2019/06/ajanloszelveny.pdf"><strong>https://ahang.hu/wp-content/uploads/2019/06/ajanloszelveny.pdf</strong></a></p>

                          <p class="alert alert-light">Az ajánlóíveket személyesen vagy megbízott útján a 1071 Budapest, Damjanich u. 52. IV. emelet 1. címre kell behozni előzetes egyeztetést követően. Az átadás-átvétel időpontját a +3670-203-3476-os telefonszámon lehet egyeztetni. A telefon hétköznapokon reggel 9 és este 6 óra között elérhető.</p>

                          <p class="alert alert-light">A jelenzkezéshez szükséges jelölti nyilatkozat az alábbi linkről tölthető le: <a target="_blank" class="text-secondary" href="https://ahang.hu/wp-content/uploads/2019/06/fpj_nyilatkozat.pdf"><strong>https://ahang.hu/wp-content/uploads/2019/06/fpj_nyilatkozat.pdf</strong></a></p>
                        </div>
                      </div>

                      <div class="accordion">
                        <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#panel-body-5" aria-expanded="false">
                          <h4>Menetrend</h4>
                        </div>
                        <div class="accordion-body collapse" id="panel-body-5" data-parent="#accordion" style="">
                          <p class="alert alert-warning">Ajánlásgyűjtő időszak jelöltek számára:</p>
                          <ul>
                            <li>2019. június 5. – június 17. 16:00</li>
                          </ul>

                          <p class="alert alert-light">A gyűjtéshez az alábbi hivatalos aláírásgyűjtő ívet kötelező használni: <a target="_blank" class="text-secondary" href="https://ahang.hu/wp-content/uploads/2019/06/Ajánlószelvény_bpelőválasztás.pdf</p>"><strong>https://ahang.hu/wp-content/uploads/2019/06/Ajánlószelvény_bpelőválasztás.pdf</p></strong></a>

                          <p class="alert alert-light">Jelöltek számára szükséges a főpolgármesterjelölti értéknyilatkozat aláírása és nyilvános, online közzététele a jelöltséghez szükséges ajánlás leadásával egy időben – további részletek a jelölttté válás pontos feltételeiről: <a target="_blank" class="text-secondary" href="https://terjed.ahang.hu/campaigns/jel%C3%B6ltek"><strong>https://terjed.ahang.hu/campaigns/jel%C3%B6ltek</strong></a></p>

                          <p class="alert alert-warning"><strong>Online szavazásra jelentkezés</strong> 2019. június 12., délelőtt 10 óra - június 20., 08:00 óra</p>
                          <ul>
                            <li>Online az ügyfélkapu használatával az elovalaszto.hu oldalon 2019. június 12., – június 19.</li>
                            <li>Személyesen (offline) az erre a célra kijelölt sátrakban: lásd, a “Hol szavazhatok?” menü alatt</li>
                          </ul>
                          <p class="alert alert-warning">Beérkezett ajánlások ellenőrzése:</p>
                          <ul>
                            <li>2019. június 12. – 2019. június 18.</li>
                          </ul>
                          <p class="alert alert-warning">Jelöltek kihirdetése:</p>
                          <ul>
                            <li>2019. június 18.</li>
                          </ul>
                          <p class="alert alert-warning">Jelölti vita:</p>
                          <ul>
                            <li>2019. június 19. Este 18:00 óra – (részletek később az aHang facebook oldalán)</li>
                          </ul>
                          <p class="alert alert-warning">Szavazás időszaka:</p>
                          <ul>
                            <li>2019. június 20., 09:00  – 2019. június 26. 12:00</li>
                          </ul>
                           <p class="alert alert-warning">Szavazás online és szavazóhelységekben:</p>
                          <ul>
                            <li>online szavazás: június 20., 09:00-június 26. 12:00</li>
                          </ul>
                          <p class="alert alert-warning">Szavazás szavazóhelységekben:</p>
                          <ul>
                          	<li>június. 20-25. 8:00-20:00 és június 26. 8:00-12:00</li>
                          </ul>
                        </div>
                      </div>

                      <div class="accordion">
                        <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#panel-body-6" aria-expanded="false">
                          <h4>Regisztráció az előválasztásra</h4>
                        </div>
                        <div class="accordion-body collapse" id="panel-body-6" data-parent="#accordion" style="">
                          <p class="alert alert-light">A szavazáshoz regisztráció szükséges. Regisztrálni offline és online egyaránt lehetséges a következő módokon:</p>
                          <p class="alert alert-warning">A, Online regisztráció az online szavazásra:</p>
                          <ul>
                            <li>Az online regisztráció az online szavazásra 2019. június 12. 08:00 és június 20. 08:00 óra között lehetséges</li>
                            <li><strong>Az online regisztrációhoz szükség van az Ügyfélkapu és az úgynevezett ’Azonosításra visszavezetett dokumentum-hitelesítés’ (AVDH) szolgáltatások használatára.</strong></li>
                            <li>Ennek megszerzéséhez az alábbi tájékoztatók nyújtanak segítséget: <a target="_blank" class="text-secondary" href="https://elovalaszto.hu/users/pdf"><strong>https://elovalaszto.hu/users/pdf</strong></a></li>
                          </ul>
                          <p class="alert alert-warning">B, Személyes (offline) regisztráció az online szavazásra:</p>
                          <ul>
                            <li>Személyes regisztráció legalább egy személyazonosságot igazoló érvényes fényképes igazolvánnyal: személyi igazolvány, útlevél, vezetői engedély, valamint érvényes budapesti lakcímkártya bemutatásával a szavazóhelységeknél lehetséges.</li>
                            <li>Az online szavazás 2019. június 20. 10:00 és június 26. 12:00 óra között lehetséges, fontos, hogy <strong>ezután már regisztrálni nem lehet személyesen az online szavazásra.</strong></li>
                          </ul>
                          <p class="alert alert-warning">C, Személyes (offline) regisztráció a személyes (offline) szavazásra:</p>
                          <ul>
                            <li>A 2019. június 20. 08:00 és június 26. 12:00 között tartó személyes (offline) szavazás időszakában a szavazással együtt a szavazóhelységeknél.</li>
                          </ul>
                        </div>
                      </div>

                      <div class="accordion">
                        <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#panel-body-7" aria-expanded="false">
                          <h4>Hol szavazhatok?</h4>
                        </div>
                        <div class="accordion-body collapse" id="panel-body-7" data-parent="#accordion" style="">
                          <p class="alert alert-light">A szavazás titkos és személyes. Minden, az előválasztásra regisztráló polgár egy szavazatot adhat le.</p>

                          <p class="alert alert-warning"><strong>Szavazóhelységek:</strong></p>

                          <ul><li>Az előválasztás szavazóhelységei a CVB tagszervezetei által üzemeltetett utcai sátrak. A szavazóhelységekben szavazóköri bizottságok működnek, amelyek teljes körűen üzemeltetik a szavazás folyamatát. A bizottságok elnöklését mindig valamely CVB-ben tag civil közösség tagja látja el, működtetését a CVB-ben tag szervezetek (civil közösségek és pártok) delegáltjai látják el.</li></ul>

                          <p class="alert alert-warning"><strong>A szavazóhelységek elérhetőségei és nyitvatartási idejük:</strong></p>

                          <ul><li><strong>Nyitvatartási idők -  2019. június 20-25. 10-20 óráig. Június 26. 12 óráig.</strong></li></ul>

                          <p class="alert alert-warning"><strong>Helyszínek:</strong></p>
                          <ul>
                            <li>Móricz Zsigmond körtér</li>
                            <li>Széll Kálmán tér</li>
                            <li>Blaha Lujza tér</li>
                            <li>Nyugati tér</li>
                            <li>Örs vezér tere</li>
                            <li>Boráros tér</li>
                            <li>Csepel, Görgey tér</li>
                            <li>XV. kerület Fő tér</li>
                            <li>Lehel tér</li>
                            <li>Nagyvárad tér (aluljáró)</li>
                            <li>Újpest Városközpont</li>
                            <li>Flórián tér</li>
                            <li>XVII. kerület, Rákoskeresztúri központ</li>
                            <li>Kispest, Határ út</li>
                            <li>Kőbánya-Kispest metróállomás</li>
                          </ul>
                        </div>
                      </div>

                      <div class="accordion">
                        <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#panel-body-8" aria-expanded="false">
                          <h4>Online szavazás</h4>
                        </div>
                        <div class="accordion-body collapse" id="panel-body-8" data-parent="#accordion" style="">
                          <p class="alert alert-light">Az online szavazáshoz szükséges online regisztrálni, felhasználói fiókot létrehozni. A szavazás 2019. június 20. 10:00 és június 26. 12:00 között az online regisztráció során létrehozott felhasználói fiókba belépve, a szavazás funkció kiválasztásával történik. Az online szavazáshoz szükséges regisztráció előfeltétele, hogy az előválasztó rendelkezzen Ügyfélkapu-s regisztrációval, valamint az Ügyfélkapuról letöltött személyes adatait tartalmazó PDF filet az AVDH szolgáltatással aláírja és feltöltse a honlapunkra.</p>

                          <p class="alert alert-info">A folyamat néhány egyszerű lépésben elvégezhető - segédletünket itt találja: <a target="_blank" href="https://elovalaszto.hu/users/pdf"><strong>https://elovalaszto.hu/users/pdf</strong></a></p>

                          <p class="alert alert-light">Az Ügyfélkapu egy állami szolgáltatás, amelyen keresztül a polgárok hivatalos ügyeket intézhetnek az interneten. Az Ügyfélkapu fiók létesítését bármely természetes személy kezdeményezheti személyesen, vagy elektronikusan, amennyiben 2016. január 1-jét követően kiállított érvényes személyazonosító igazolvánnyal rendelkezik. <a target="_blank" class="text-secondary" href="https://ugyfelkapu.gov.hu/sugo"><strong>https://ugyfelkapu.gov.hu/sugo</strong></a></p>

                          <p class="alert alert-light">Amennyiben nem rendelkezik az online szavazáshoz szükséges Ügyfélkapu-s regisztrációval, a regisztrációt kezdeményezheti a magyarorszag.hu weboldalon, okmányirodában, kormányablakban, az adóhatóság kiemelt ügyfélszolgálatain, egyes postai ügyfélszolgálatokon vagy külképviseleteken személyesen. Bővebben az Ügyfélkapu regisztrációról: <a target="_blank" class="text-secondary" href="https://ugyfelkapu.gov.hu/sugo/s-regisztracio/s-szemelyes-regisztracio"><strong>https://ugyfelkapu.gov.hu/sugo/s-regisztracio/s-szemelyes-regisztracio</strong></a></p>
                          
                          <p class="alert alert-light">Kormányablakok Budapesten <a target="_blank" class="text-secondary" href="http://kormanyablak.hu/hu/kormanyablakok/budapest/1"><strong>http://kormanyablak.hu/hu/kormanyablakok/budapest/1</strong></a></p>
                          <p class="alert alert-light">Időpont foglalása kormányablakba <a target="_blank" class="text-secondary" href="https://ugyfelkapu.gov.hu/regisztracio/regEszemelyi#ancor"><strong>https://ugyfelkapu.gov.hu/regisztracio/regEszemelyi#ancor</strong></a></p>
                          <p class="alert alert-light">Az AVDH itt érthető el <a target="_blank" class="text-secondary" href="https://niszavdh.gov.hu/index"><strong>https://niszavdh.gov.hu/index</strong></a></p>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

          </div>
        </section>